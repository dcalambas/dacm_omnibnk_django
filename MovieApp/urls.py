from django.urls import path
from . import views
from .views import (
    MovieListView,
    MovieDetailView,
    MovieCreateView,
    MovieUpdateView,
    MovieDeleteView)
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.home, name='movies-home'),
    path('register/', views.register, name='movies-signup'),
    path('movies/', MovieListView.as_view(), name='movies-list'),
    path('movies/movie/<pk>/', MovieDetailView.as_view(), name='movie-detail'),
    path('movies/create/', MovieCreateView.as_view(), name='movie-create'),
    path('movies/<pk>/update/', MovieUpdateView.as_view(), name='movie-update'),
    path('movies/<pk>/delete/', MovieDeleteView.as_view(), name='movie-delete'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

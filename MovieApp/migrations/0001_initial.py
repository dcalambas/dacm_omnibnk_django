# Generated by Django 2.2 on 2019-04-26 01:25

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.PositiveIntegerField(primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=35, blank=False)),
                ('genre', models.CharField(max_length=35, blank=False)),
                ('classification', models.CharField(max_length=35, blank=False)),
                ('score', models.PositiveSmallIntegerField(blank=False)),
                ('poster', models.ImageField(default='default.jpg', upload_to='movie_pics')),
            ],
        ),
    ]

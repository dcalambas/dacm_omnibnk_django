from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from .forms import UserRegisterForm
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView)
from .models import Movie


def home(request):
    context = {
        'movies': Movie.objects.all().filter(score__gt=3).order_by('-score','title')
    }
    return render(request, 'MovieApp/home.html', context)


def login(request):
    return render(request, 'MovieApp/login.html')


def movies_list(request):
    context = {
        'movies': Movie.objects.all()
    }
    return render(request, 'MovieApp/movies-list.html', context)


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}! {username} is now able to log in')
            return redirect('movies-login')
    else:
        form = UserRegisterForm()
    return render(request, 'MovieApp/register.html', {'form':form})


class MovieListView(ListView):
    model = Movie
    template_name = 'MovieApp/movies-list.html'
    context_object_name = 'movies'
    ordering = ['title']


class MovieDetailView(DetailView):
    model = Movie
    template_name = 'MovieApp/movies-detail.html'
    context_object_name = 'movie'


class MovieCreateView(LoginRequiredMixin, CreateView):
    model = Movie
    fields = ['id', 'title', 'genre', 'classification', 'score', 'poster']
    fields_required = ['id', 'title', 'genre', 'classification', 'score']
    template_name = 'MovieApp/movies-form.html'


class MovieUpdateView(LoginRequiredMixin, UpdateView):
    model = Movie
    fields = ['id', 'title', 'genre', 'classification', 'score', 'poster']
    template_name = 'MovieApp/movies-form.html'


class MovieDeleteView(LoginRequiredMixin, DeleteView):
    model = Movie
    success_url = '/'

from django.db import models
from django.urls import reverse

# Create your models here.

class Movie(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    title = models.CharField(max_length=35, blank=False)
    genre = models.CharField(max_length=35, blank=False)
    classification = models.CharField(max_length=35, blank=False)
    score = models.PositiveSmallIntegerField(blank=False)
    poster = models.ImageField(default='default.jpg', upload_to='movie_pics')

    def __str__(self):
        movie = "{0}"
        return movie.format(self.title)

    def get_absolute_url(self):
        return reverse('movie-detail', kwargs={'pk': self.pk})
